package producerConsumer;

public class Consumatore extends Thread
{
	private BufferCondiviso tmpBuffer;

	public Consumatore(BufferCondiviso buffer)
	{
		tmpBuffer = buffer;  //salvo il riferimento al buffer condiviso
	}
	
	public void run()
	{
		for(int i = 0; i < 20; i ++)
		{
			Integer tmp = tmpBuffer.consuma();
			System.out.println("CONSUMATORE: Ho letto un dato dal buffer = " + tmp.toString());
		}
		
		System.out.println("CONSUMATORE: Termino esecuzione");
	}
}
