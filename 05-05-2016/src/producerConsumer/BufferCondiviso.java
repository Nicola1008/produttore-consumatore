package producerConsumer;

import java.util.ArrayDeque;

public class BufferCondiviso
{
	private ArrayDeque<Integer> buffer = new ArrayDeque<>();
	private final int DIM_MAX = 10;
	
	public void produci(Integer num)
	{
		synchronized (buffer)
		{
			if(buffer.size() == DIM_MAX)  //se il buffer � pieno
			{
				System.out.println("PRODUTTORE: Il BUFFER � pieno... Eseguo una WAIT()");
				try
				{
					wait();  //resto in attesa
				} 
				catch (InterruptedException e)
				{
					System.out.println("PRODUTTORE: Ho ricevuto una notify()");
				}
			}
			System.out.println("PRODUTTORE: Inserisco dato");
			buffer.addFirst(num);  //scrivo il numero
			notify();  //notifico al consumatore che se vuole c'� un dato da prelevare
		}
	}
	
	public Integer consuma()
	{
		synchronized (buffer)
		{
			if(buffer.isEmpty())  //se il buffer � vuoto
			{
				System.out.println("CONSUMATORE: Il BUFFER � vuoto... Eseguo una WAIT()");
				try
				{
					wait();  //aspetto che qualcuno scriva dei dati nel buffer
				} 
				catch (InterruptedException e)
				{
					System.out.println("CONSUMATORE: Ho ricevuto una notify()");
				}
			}
			
			Integer tmp = buffer.removeLast();
			notify();  //notifico ad un eventuale produttore fermo ad aspettare che il buffer si svuotasse
			return tmp;
			
		}
	}

	
	
	public static void main(String[] args)
	{
		BufferCondiviso buff = new BufferCondiviso();
		Produttore produttore = new Produttore(buff);
		Consumatore consumatore = new Consumatore(buff);
		
		produttore.start();
		consumatore.start();
			
	}

}
