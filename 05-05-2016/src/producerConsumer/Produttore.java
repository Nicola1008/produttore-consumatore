package producerConsumer;

public class Produttore extends Thread
{
	private BufferCondiviso tmpBuffer;
	static Integer n = 1;
	
	public Produttore(BufferCondiviso buffer)
	{
		tmpBuffer = buffer;  //salvo il riferimento al buffer condiviso
	}
	
	public void run()
	{
		for(int i = 0; i < 20; i ++)
			tmpBuffer.produci(n++);
		
		System.out.println("PRODUTTORE: Termino esecuzione");
		
	}
}	
