package bankAccount;

public class ThreadA extends Thread 
{
	private BankAccount sharedAccount;
	
	public ThreadA(BankAccount account)
	{
		sharedAccount = account;
	}

	@Override
	public void run() 
	{
		try 
		{
			Thread.sleep(100);  //attendo 1/10 di secondo
		} 
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		sharedAccount.withdraw(100);  //prelevo 100�
	}	
}

