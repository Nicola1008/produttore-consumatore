package bankAccount;

public class ThreadB extends Thread 
{
private BankAccount sharedAccount;
	
	public ThreadB(BankAccount account)
	{
		sharedAccount = account;
	}

	@Override
	public void run() 
	{
		try 
		{
			Thread.sleep(100);  //attendo 1/10 di secondo
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		sharedAccount.deposit(50);  //deposito 50�
	}	
}
