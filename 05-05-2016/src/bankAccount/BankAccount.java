package bankAccount;

public class BankAccount 
{
	private Integer balance;
	
	public BankAccount(int initialBalance)
	{
		this.balance = initialBalance;  //saldo iniziale
	}

	/**
	 * permette di effettuare un deposito
	 */
	public void deposit(int value)
	{
		synchronized (balance)
		{
			int lastBalance = balance;
			int newBalance = lastBalance + value;
			balance = newBalance;
		}
	}
	
	/**
	 * permette di effettuare un cashout
	 * @param value = ci� che si intende prelevare
	 * @return i soldi che si � chiesto di prelevare se sono disponibili sul conto, altrimenti -1
	 */
	public int withdraw(int value)
	{
		synchronized (balance)
		{
			if(balance >= value)
			{
				int lastBalance = balance;
				int newBalance = lastBalance - value;
				balance = newBalance;
				return value;
			}
			return -1;
		}
	}
	
	public int getBalance()
	{
		synchronized (balance)
		{
			return balance;
		}
	}
	
	public static void main(String[] args)
	{
		BankAccount account = new BankAccount(1000);  //creo un nuovo conto partendo da 1000�
	
		Thread a = new ThreadA(account);
		Thread b = new ThreadB(account);
		a.start();
		b.start();
		
		try 
		{
			a.join();
			b.join();
		} catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		
		System.out.println("Balance: " + account.getBalance());
	}

}
